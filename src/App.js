import './App.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {useState} from "react";
import {toast, ToastContainer} from "react-toastify";

function App() {

    const [selectedDate, setSelectedDate] = useState(null);
    const [selectedTime, setSelectedTime] = useState(0);
    const [isAvailable, setIsAvailable] = useState(false);

    const dateChange = (date) => {
        setSelectedDate(date);
    };

    const timeChange = (event) => {
        setSelectedTime(event.target.value);
    };

    const timeSlots = [];
    const startHour = 7;
    const endHour = 22;

    for (let i = startHour; i < endHour; i++) {
        timeSlots.push(`${i}:00`);
        timeSlots.push(`${i}:30`);
    }

    const checkIsAvailable = () => {
        // Faire plus de vérification, notamment sur le time
        if (selectedDate) {

            //Mettre ce bout de code dans une fonction à part
            const year = selectedDate.getFullYear();
            const month = ("0" + (selectedDate.getMonth() + 1)).slice(-2);
            const day = ("0" + selectedDate.getDate()).slice(-2);
            const formattedDate = `${year}-${month}-${day}`;
            const dateTime = `${formattedDate}T${selectedTime}:00Z`;

            fetch(`http://localhost:8080/resource/1337/available?datetime=${dateTime}`)
                .then((response) => response.json())
                .then((data) => {
                    setIsAvailable(data["available"]);
                    console.log(data["available"]);
                    console.log(isAvailable);
                    toast(isAvailable);
                })
                .catch((error) => console.error(error));
        } else {
            console.error("No date");
        }
    };

    return (
        <div className="App">
            <h1>Check if the resource is available</h1>
            <DatePicker selected={selectedDate} onChange={dateChange} dateFormat="dd/MM/yyyy"/>
            <select value={selectedTime} onChange={timeChange}>
                <option value="">Select a time</option>
                {timeSlots.map((time) => (
                    <option key={time} value={time}>
                        {time}
                    </option>
                ))}
            </select>
            <button onClick={checkIsAvailable}>Check availability</button>
            <ToastContainer />
        </div>
    );
}

export default App;
